include<stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int calc(int a,int b[10])
{
    int i=0;
    while(a!=0)
    {
        b[i]=a%2;
        a=a/2;
        i++;
    }
    return i;
}
void output(int a,int b[10],int c)
{
    printf("%d in binary is ",a);
    for(int i=c-1;i>=0;i--)
        printf("%d",b[i]);
    printf("\n");
}
int main()
{
    int a,b[10],c;
    a=input();
    c=calc(a,b);
    output(a,b,c);
    return 0;
}

